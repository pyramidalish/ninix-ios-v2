//
//  NXVitalSignModel.swift
//  ninix
//
//  Created by Ali on 4/22/17.
//  Copyrhs © 2017 ninix. All rhss reserved.
//

import Foundation
import RealmSwift

class NXVitalSignModel: NXSyncableModel {
    
    dynamic var temperature: Float = 0.0
    dynamic var humidity: Int = 0
    dynamic var respiratory: Int = 0
    dynamic var orientation: Int = 0
    dynamic var lastsFor: Double = 0
    dynamic var isOffline: Bool = false
    
    
    public static func ==(lhs: NXVitalSignModel, rhs: NXVitalSignModel) -> Bool {
        return lhs.temperature == rhs.temperature && lhs.humidity == rhs.humidity && lhs.respiratory == rhs.respiratory && lhs.orientation == rhs.orientation
    }
    
    static func !=(lhs: NXVitalSignModel, rhs: NXVitalSignModel) -> Bool {
        return !(lhs == rhs)
    }
    
    
}
