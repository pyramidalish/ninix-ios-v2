//
//  NXCredentialModel.swift
//  ninix
//
//  Created by Ali on 4/24/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation

class NXCredentialModel: NSObject, NSCoding {
    
    var username: String?
    var password: String?
    
    init(username: String, password: String) {
        self.username = username
        self.password = password
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.username = aDecoder.decodeObject(forKey: "username") as? String
        self.password = aDecoder.decodeObject(forKey: "password") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.username, forKey: "username")
        aCoder.encode(self.password, forKey: "password")
    }
    
    func save() {
        NXSecureStorage.set(self, for: .credential)
    }
    
}
