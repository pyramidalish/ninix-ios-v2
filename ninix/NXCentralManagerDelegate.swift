//
// Created by Ali on 4/19/17.
// Copyright (c) 2017 ninix. All rights reserved.
//

import Foundation
import CoreBluetooth


@objc protocol NXCentralManagerDelegate {

    @objc optional func didDiscoverPeripheral()
    @objc optional func didConnect()
    @objc optional func didFailToConnect(_ error: Error?)
    @objc optional func didDisconnect()
    @objc optional func didScanFail(_ centralState: CBManagerState)
    func centralManagerDidUpdateState(_ central: CBCentralManager)

}