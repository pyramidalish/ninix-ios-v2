//
//  NXNotificationModel.swift
//  ninix
//
//  Created by Ali on 4/22/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import RealmSwift

class NXNotificationModel: NXSyncableModel {
 
    dynamic var type: Int = 0
    dynamic var status: Int = 0
    dynamic var value: Float = 0.0
    dynamic var seen: Bool = false
    
}
