//
//  NXLoginModel.swift
//  ninix
//
//  Created by Ali on 4/22/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import ObjectMapper

struct NXLoginModel: Mappable {
    
    var username: String?
    var password: String?
    
    init(username: String, password: String) {
        self.username = username
        self.password = password
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        username <- map["username"]
        password <- map["password"]
    }
    
}
