//
//  .swift
//  ninix
//
//  Created by Ali on 4/26/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import RealmSwift

class NXDataStoreHandler: NSObject {
    
    var vitalSignModelStack: [NXVitalSignModel] = []
    
    func save(_ vitalSignModel: [NXVitalSignModel]) {
        DispatchQueue(label: "ir.ninixco.ninix.save", qos: .utility, attributes: .concurrent, autoreleaseFrequency: .inherit, target: nil).async {
            self.vitalSignModelStack += vitalSignModel
            do {
                let realm = try Realm()
                do {
                    try realm.write {
                        for (index, data) in self.vitalSignModelStack.enumerated() {
                            realm.add(data)
                            self.vitalSignModelStack.remove(at: index)
                        }
                    }
                }
                catch let e as NSError {
                    print(e, "error occured when saving data")
                }
            }
            catch let e as NSError {
                print(e, "error occured to initilize realm")
            }
        }
        
    }
    
    
}
