//
//  NXLogin.swift
//  ninix
//
//  Created by Ali on 4/22/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import Alamofire

class NXLoginHandler: NSObject, NXHttpPost {
    
    typealias ResultModel = NXAccessTokenModel
    
    internal var needAuth: Bool
    var url: URL
    var parameters: Parameters?
    var delegate: NXAuthenticationDelegate?
    
    init(loginModel: NXLoginModel) {
        self.url = URL(string: NXSetting.NXHttp.NXAddress.login)!
        self.parameters = loginModel.toJSON()
        self.needAuth = false
        super.init()
    }
    
    func onSuccessResponse(response: ResultModel) {
        response.save()
        delegate?.success()
    }
    
    func onFailureResponse(error: NXJsonResponse<ResultModel>) {
        // TODO: handle unsuccessful login
        print("failure response", error.messageEn ?? "no message", #function)
        delegate?.failure(error: error)
    }
    
    func onServerFail(_ error: String) {
        // TODO: handle server fail in login
        print("server fail", error, #function)
        delegate?.serverFailure(error: error)
    }
    
}

