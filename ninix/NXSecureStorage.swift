//
//  NXSecureStorage.swift
//  ninix
//
//  Created by Ali on 4/24/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import KeychainSwift

class NXSecureStorage: NSObject {
    
    static let keyChain: KeychainSwift = KeychainSwift()
    
    static func set(_ value: Any, for key: NXSecureStorageKey) {
        let encoded = NSKeyedArchiver.archivedData(withRootObject: value)
        self.keyChain.set(encoded, forKey: key.rawValue)
    }
    
    static func get(_ key: NXSecureStorageKey) -> Any? {
        if let encoded = self.keyChain.getData(key.rawValue) {
            return NSKeyedUnarchiver.unarchiveObject(with: encoded)
        }
        return nil
    }
    
    static func remove(_ key: NXSecureStorageKey) {
        self.keyChain.delete(key.rawValue)
    }
    
    static func getCredential() -> NXCredentialModel? {
        return self.get(.credential) as? NXCredentialModel
    }
    
    static func getToken() -> NXAccessTokenModel? {
        return self.get(.token) as? NXAccessTokenModel
    }
    
    
    
}


enum NXSecureStorageKey: String {
    case credential, token
}
