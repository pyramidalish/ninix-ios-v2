//
//  NXCentralManager.swift
//  ninix
//
//  Created by Ali on 4/19/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import CoreBluetooth

class NXCentralManager: NSObject, CBCentralManagerDelegate {

    static let shared: NXCentralManager = NXCentralManager()
    
    var centralManager: CBCentralManager!
    var discoveredDevices: [NXDiscoveredDevice] = []
    var timer: Timer!
    var delegate: NXCentralManagerDelegate?
    var scanStatus: Bool = false
    var discoveryTime: TimeInterval!

    private override init() {
        super.init()
        let queue = DispatchQueue(label: "ir.ninixco.central", qos: .background, attributes: .concurrent, autoreleaseFrequency: .workItem, target: nil)
        self.centralManager = CBCentralManager(delegate: self, queue: queue)
        self.discoveryTime = NXSetting.NXBluetooth.discoveryInterval
    }
    
    /* -------------------------------------- Scan methods ------------------------------------------- */
    
    // MARK: start scan peripherals
    public func scanPeripherals() {
        self.scanStatus = true
        self.timer = Timer.scheduledTimer(timeInterval: discoveryTime, target: self, selector: #selector(NXCentralManager.checkDiscoveredDevices(_:)), userInfo: nil, repeats: true)
        self.scan()
    }

    // MARK: stop scan devices
    public func stopScan() {
        self.centralManager.stopScan()
        self.timer.invalidate()
        self.discoveredDevices.removeAll()
        self.scanStatus = false
    }
    
    private func scan() {
        switch self.centralManager.state {
        case .poweredOn:
            self.centralManager.scanForPeripherals(withServices: [NXServiceUUID.main])
        default:
            self.delegate?.didScanFail?(self.centralManager.state)
        }
    }
    
    
    /* --------------------------------------- Connectivity methods --------------------------------------- */
    
    // MARK: Disconnect from device
    public func disconnect() {
        if let peripheral = NXPeripheral.shared.peripheral {
            NXPeripheral.shared.disconnect()
            self.centralManager.cancelPeripheralConnection(peripheral)
        }
    }
    
    // MARK: Connect to device
    func connect(_ device: NXDiscoveredDevice) {
        self.centralManager.connect(device.peripheral, options: nil)
    }
    
    /* ------------------------------------------  timer function ------------------------------------------ */
    
    func checkDiscoveredDevices(_ sender: Any) {
        
        let time = Date().timeIntervalSince1970
        for (index, device) in self.discoveredDevices.enumerated() {
            if device.time < time - discoveryTime * 2 {
                self.discoveredDevices.remove(at: index)
                self.delegate?.didDiscoverPeripheral?()
            }
        }
        self.scan()
    }
    
    /* ------------------------------------------ CBCentralManagerDelegate Events ------------------------------------------ */
    
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        self.delegate?.centralManagerDidUpdateState(central)
        if self.scanStatus {
            self.scan()
        }
    }

    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String: Any], rssi RSSI: NSNumber) {

        if !self.timer.isValid {
            self.timer.fire()
        }
        
        let localNameKey = advertisementData[CBAdvertisementDataLocalNameKey]
        let time = Date().timeIntervalSince1970
        let discoveredDevice = NXDiscoveredDevice(name: localNameKey, time: time, rssi: RSSI, peripheral: peripheral)
        
        if let index = self.discoveredDevices.index(where: { $0 == discoveredDevice}) {
            self.discoveredDevices[index] = discoveredDevice
            return
        }
        
        self.discoveredDevices.append(discoveredDevice)
        self.delegate?.didDiscoverPeripheral?()

    }

    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("connected")
        NXPeripheral.shared.didConnect(peripheral)
        self.delegate?.didConnect?()
        self.stopScan()
    }

    public func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {

        self.delegate?.didFailToConnect?(error)
        
    }

    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
        NXPeripheral.shared.didDisconnect()
        self.delegate?.didDisconnect?()
        
    }

}
