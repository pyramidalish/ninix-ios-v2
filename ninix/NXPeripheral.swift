//
//  NXPeripheral.swift
//  ninix
//
//  Created by Ali on 4/19/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import CoreBluetooth

class NXPeripheral: NSObject, CBPeripheralDelegate {

    static let shared: NXPeripheral = NXPeripheral()
    
    var peripheral: CBPeripheral?
    var UUID: UUID?
    
    var servicesUUID: [CBUUID] = [NXServiceUUID.main]
    var notifiedCharacteristics: [CBCharacteristic] = []
    var bluetoothDataHandler: NXBluetoothDataHandler

    private override init() {
        self.bluetoothDataHandler = NXBluetoothDataHandler()
        super.init()
    }
    
    // MARK: Disconnect from peripheral
    /*
     * removed notified characteristics
     */
    public func disconnect() {
        for characteristic in self.notifiedCharacteristics {
            self.peripheral?.setNotifyValue(false, for: characteristic)
        }
    }
    
    // MARK: Discover services method
    public func discoverServices() {
        self.peripheral?.discoverServices([NXServiceUUID.main])
    }
    
    // MARK: Write data to characteristic
    func writeData(data: Data) {
        for characteristic in self.notifiedCharacteristics {
            if characteristic.uuid == NXCharacteristicUUID.sendData {
                self.peripheral?.writeValue(data, for: characteristic, type: .withResponse)
            }
        }
    }

    /* --------------------------------------- NXCentralManager Events --------------------------------------- */
    
    // MARK: when peripheral connected
    /*
     * set peripheral
     * set delegate to receive events
     * save peripheral identifier
     * discover services
     */
    public func didConnect(_ peripheral: CBPeripheral) {
        
        peripheral.delegate = self
        
        self.peripheral = peripheral
        self.UUID = peripheral.identifier
        self.discoverServices()
    }
    
    
    // MARK: when peripheral disconnect
    /*
     * remove periphearl reference
     * remove saved uuid
     * remove all characteristic notification
     * save last data in bluetooth data handler stack
     */
    public func didDisconnect() {
        
        self.peripheral = nil
        self.UUID = nil
        self.notifiedCharacteristics.removeAll()
        self.bluetoothDataHandler.saveLastData()
        
    }
    
    /* ------------------------------------- CBPeripheral Events ------------------------------------- */

    public func peripheralDidUpdateName(_ peripheral: CBPeripheral) {

        // MARK: peripheral name changed

    }

    public func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]) {

        // MARK: peripheral service changed

    }

    public func peripheralDidUpdateRSSI(_ peripheral: CBPeripheral, error: Error?) {

        // MARK: peripheral rssi changed

    }

    public func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {

        // MARK: did read rssi

    }

    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        if let services = peripheral.services {
            for service in services {
                if self.servicesUUID.contains(service.uuid) {
                    peripheral.discoverCharacteristics(nil, for: service)
                }
            }
        }

    }

    public func peripheral(_ peripheral: CBPeripheral, didDiscoverIncludedServicesFor service: CBService, error: Error?) {

        // MARK: did discover included services

    }

    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        
        if let error = error {
            print(error, #function)
            return
        }
        
        if let characteristics = service.characteristics {
            
            for characteristic in characteristics {
                
                if NXCharacteristicUUID.readCharacteristics.contains(characteristic.uuid) {
                    peripheral.readValue(for: characteristic)
                    peripheral.setNotifyValue(true, for: characteristic)
                }
                self.notifiedCharacteristics.append(characteristic)
                
            }
        }

    }

    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {

        if let error = error {
            print(error, #function)
            return
        }
        
        self.bluetoothDataHandler.didUpdateValueFor(characteristic)

    }

    public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        
        if let error = error {
            print(error.localizedDescription, #function)
            return
        }
        
        // TODO: handle response from write characteristic
    }

    public func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {

        if let error = error {
            print(error, #function)
            return
        }

    }

    public func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?) {

        // MARK: did discover descriptor

    }

}
