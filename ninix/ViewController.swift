//
//  ViewController.swift
//  ninix
//
//  Created by Ali on 4/19/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController, NXCentralManagerDelegate {

    var nxCentralManager: NXCentralManager = NXCentralManager.shared
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        nxCentralManager.delegate = self
        nxCentralManager.scanPeripherals()

        print(Date().timeIntervalSince1970)
 
        /*
        let user = NXUserModel()
        user.address = "Shahriyar"
        user.birthdate = Date()
        user.children = 0
        user.email = "pyramidalish@gmail.com"
        user.fullName = "Ali Shabani"
        user.phone = "123123123123"
        user.relation = NXRelation.father
        user.username = "username"
        print(user.toJSONString() ?? "nil", "user josn")
        
        
        let encoded = NSKeyedArchiver.archivedData(withRootObject: user)
        UserDefaults.standard.set(encoded, forKey: "user")
        
        
        if let decoded = UserDefaults.standard.data(forKey: "user") {
            
            
            if let retUser = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? NXUserModel {
                print(retUser.toJSONString() ?? "nil", "ret user")
            }
            
            
        }
        
 
        
        let signup = NXSignupModel(mobile: "09393924230", password: "123456", fullName: "ali shabani", deviceSerial: "jkjfkjfdjdfjkfdskjfdkjdfkjfsj")
        let signupHandler = NXSignupHandler(signupModel: signup)
        signupHandler.request()
        */
        //let accessToken = "{\"refresh_token\": \"ae69ec84-5511-4e91-9cf4-e6730928f93b\", \"expires_in\": 86400, \"token_type\": \"Bearer\", \"access_token\": \"c30fe9ab-b508-4575-b1c0-c58c7d02b560\"}"
        /*
        let res = "{\"result\": null, \"message_fa\": \"farsi\", \"code\": 0, \"message_en\": \"Ok\"}"
        
        let json = NXJsonResponse<NXAccessTokenModel>(JSONString: res)
        print(json)
 
        
        let login = NXLoginModel(username: "09304900220", password: "123456")
        let loginHandler = NXLoginHandler(loginModel: login)
        loginHandler.request()
        */
        let access = NXSecureStorage.getToken()
        print(access ?? "no access token", "access")
        NXBluetoothDataHandler.checkRealmFileSize()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("state changed")
    }
    
    func didDiscoverPeripheral() {
        print("did discover device", nxCentralManager.discoveredDevices)
        
        for device in nxCentralManager.discoveredDevices {
            print(device, device.name)
            if device.name == "Ninix" {
                print("connect")
                nxCentralManager.connect(device)
            }
        }
    }
    
    func didConnect() {
        print("did connect")
    }
    
    func didFailToConnect(_ error: Error?) {
        print("did failt to connect")
    }
    
    func didDisconnect() {
        print("did disconnect")
    }
    
    func didScanFail(_ centralState: CBManagerState) {
        print("did scan fail")
    }
    
    func generateData() -> Data {
        
        var data: [UInt8] = Array(repeating: 0, count: 20)
        
        for (index, _) in data.enumerated() {
            let random = UInt8(arc4random() % 256)
            data[index] = random
        }
        
        return Data(bytes: data)
        
    }


}

