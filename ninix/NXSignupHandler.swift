//
//  NXSignup.swift
//  ninix
//
//  Created by Ali on 4/22/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import Alamofire

class NXSignupHandler: NSObject, NXHttpPost {
    
    typealias ResultModel = NXAccessTokenModel
    
    internal var needAuth: Bool
    var url: URL
    var parameters: Parameters?
    var delegate: NXAuthenticationDelegate?
    
    init(signupModel: NXSignupModel) {
        self.url = URL(string: NXSetting.NXHttp.NXAddress.signup)!
        self.parameters = signupModel.toJSON()
        self.needAuth = false
        super.init()
    }
    
    func onSuccessResponse(response: ResultModel) {
        response.save()
        delegate?.success()
    }
    
    func onFailureResponse(error: NXJsonResponse<ResultModel>) {
        
        print("failure response", error.messageEn ?? "no message", #function)
        delegate?.failure(error: error)
    }
    
    func onServerFail(_ error: String) {
        
        print("server fail", error, #function)
        delegate?.serverFailure(error: error)
    }
    
}

protocol NXAuthenticationDelegate {
    
    func success()
    func failure(error: NXJsonResponse<NXAccessTokenModel>)
    func serverFailure(error: String)
    
}
