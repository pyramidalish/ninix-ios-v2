//
//  NXbabyHandler.swift
//  ninix
//
//  Created by Ali on 4/26/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import Alamofire

class NXBabyHandler: NSObject, NXHttpPost, NXHttpGet {
    
    typealias ResultModel = NXBabyModel
    
    var method: HTTPMethod
    var parameters: Parameters?
    var needAuth: Bool
    var url: URL
    
    init(method: HTTPMethod, parameters: NXBabyModel? = nil) {
        self.needAuth = true
        self.url = URL(string: NXSetting.NXHttp.NXAddress.babySync)!
        self.method = method
        self.parameters = parameters?.toJSON()
        super.init()
    }
    
    func urlRequest() -> URLRequest? {
        
        switch self.method {
        case .get:
            return self.getUrlRequest()
        case .post:
            return self.postUrlRequest()
        default:
            return nil
        }
        
    }
    
    func get() -> NXBabyHandler {
        self.method = .get
        return self
    }
    
    func post(parameters: NXBabyModel) -> NXBabyHandler {
        self.parameters = parameters.toJSON()
        self.method = .post
        return self
    }
    
    
    func onSuccessResponse(response: ResultModel) {
        response.save()
    }
    
    func onFailureResponse(error: NXJsonResponse<ResultModel>) {
        print("failure response", error.messageEn ?? "no message", #function)
    }
    
    func onServerFail(_ error: String) {
        print("server fail", error, #function)
    }
    
}
