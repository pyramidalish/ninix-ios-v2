//
//  NXAnalyseDataHandler.swift
//  ninix
//
//  Created by Ali on 4/26/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation

class NXAnalyseDataHandler: NSObject {
    
    var cTemperature: Float?
    var cRespiratory: Int?
    
    func analyseRealTimeData(_ vitalSign: NXVitalSignModel) {
        
    }
    
    func analyseOfflineData(_ vitalSigns: [NXVitalSignModel]) {
        
    }
    
}
