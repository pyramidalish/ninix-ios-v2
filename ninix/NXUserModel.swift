//
//  NXUser.swift
//  ninix
//
//  Created by Ali on 4/23/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import ObjectMapper

class NXUserModel: NSObject, NSCoding, Mappable {
    
    var fullName: String?
    var username: String?
    var birthdate: Date?
    var children: Int?
    var email: String?
    var relation: NXRelation = .none
    var phone: String?
    var address: String?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.fullName = aDecoder.decodeObject(forKey: "fullname") as? String
        self.username = aDecoder.decodeObject(forKey: "username") as? String
        self.birthdate = aDecoder.decodeObject(forKey: "birthdate") as? Date
        self.children = aDecoder.decodeObject(forKey: "children") as? Int
        self.email = aDecoder.decodeObject(forKey: "email") as? String
        self.relation = NXRelation(rawValue: (aDecoder.decodeInteger(forKey: "relation"))) ?? .none
        self.phone = aDecoder.decodeObject(forKey: "phone") as? String
        self.address = aDecoder.decodeObject(forKey: "address") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.fullName, forKey: "fullname")
        aCoder.encode(self.username, forKey: "username")
        aCoder.encode(self.birthdate, forKey: "birthdate")
        aCoder.encode(self.children, forKey: "children")
        aCoder.encode(self.email, forKey: "email")
        print(self.relation.rawValue, "relation")
        aCoder.encode(self.relation.rawValue, forKey: "relation")
        aCoder.encode(self.phone, forKey: "phone")
        aCoder.encode(self.address, forKey: "address")
    }
    
    
    func mapping(map: Map) {
        fullName <- map["fullname"]
        username <- map["username"]
        birthdate <- (map["birthdate"], DateTransform())
        children <- map["children"]
        email <- map["email"]
        relation <- map["relation"]
        phone <- map["phone"]
        address <- map["address"]
        
    }
    
    func save() {
        NXStorage.set(self, for: .user)
    }
    
}

enum NXRelation: Int {
    case father = 0
    case mother = 1, none = 2
}
