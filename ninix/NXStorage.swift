//
//  NXStorage.swift
//  ninix
//
//  Created by Ali on 4/23/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation

class NXStorage: NSObject {
    
    
    static func set(_ value: Any, for key: NXStorageKey) {
        let encoded = NSKeyedArchiver.archivedData(withRootObject: value)
        UserDefaults.standard.set(encoded, forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    static func get(_ key: NXStorageKey) -> Any? {
        if let encoded = UserDefaults.standard.data(forKey: key.rawValue) {
            return NSKeyedUnarchiver.unarchiveObject(with: encoded)
        }
        return nil
    }
    
    static func getUser() -> NXUserModel {
        return self.get(.user) as? NXUserModel ?? NXUserModel()
    }
    
    static func getBaby() -> NXBabyModel {
        return self.get(.baby) as? NXBabyModel ?? NXBabyModel()
    }
    
    
}


enum NXStorageKey: String {
    case user, baby
}
