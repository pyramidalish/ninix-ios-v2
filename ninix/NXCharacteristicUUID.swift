//
// Created by Ali on 4/19/17.
// Copyright (c) 2017 ninix. All rights reserved.
//

import Foundation
import CoreBluetooth

struct NXCharacteristicUUID {


    static var realTimeData: CBUUID {
        get {
            return CBUUID(string: "00001524-0000-1000-8000-00805f9b34fb")
        }
    }
    
    static var offlineData: CBUUID {
        get {
            return CBUUID(string: "00001525-0000-1000-8000-00805f9b34fb")
        }
    }
    static var sendData: CBUUID {
        get {
            return CBUUID(string: "00001526-0000-1000-8000-00805f9b34fb")
        }
    }
    
    static var readCharacteristics: [CBUUID] {
        get {
            return [self.realTimeData, self.offlineData]
        }
    }

}
