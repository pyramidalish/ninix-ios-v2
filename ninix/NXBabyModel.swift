//
//  NXBaby.swift
//  ninix
//
//  Created by Ali on 4/23/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import ObjectMapper

class NXBabyModel: NSObject, NSCoding, Mappable {
    
    var name: String?
    var birthdate: Date?
    var weight: Int?
    var height: Int?
    var head: Int?
    var gender: NXGender = .none
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.name = aDecoder.decodeObject(forKey: "name") as? String
        self.birthdate = aDecoder.decodeObject(forKey: "birthdate") as? Date
        self.weight = aDecoder.decodeObject(forKey: "weight") as? Int
        self.height = aDecoder.decodeObject(forKey: "height") as? Int
        self.head = aDecoder.decodeObject(forKey: "head") as? Int
        self.gender = NXGender.init(rawValue: aDecoder.decodeObject(forKey: "gender") as? Int ?? 2) ?? .none
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.birthdate, forKey: "birthdate")
        aCoder.encode(self.weight, forKey: "weight")
        aCoder.encode(self.height, forKey: "height")
        aCoder.encode(self.head, forKey: "head")
        aCoder.encode(self.gender.rawValue, forKey: "gender")
    }
    
    func mapping(map: Map) {
        name      <- map["name"]
        birthdate <- (map["birthdate"], DateTransform())
        weight    <- map["weight"]
        height    <- map["height"]
        head      <- map["head"]
        gender    <- map["gender"]
    }
    
    func save() {
        NXStorage.set(self, for: .baby)
    }
    
}


enum NXGender: Int {
    case boy = 0
    case girl, none
}
