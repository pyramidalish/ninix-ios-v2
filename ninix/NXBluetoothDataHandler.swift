//
//  NXBluetoothDataHandler.swift
//  ninix
//
//  Created by Ali on 4/26/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import CoreBluetooth
import RealmSwift


class NXBluetoothDataHandler: NSObject {
    
    var dataStoreHandler: NXDataStoreHandler = NXDataStoreHandler()
    var analyseDataHandler: NXAnalyseDataHandler = NXAnalyseDataHandler()
    var offlineData: [NXVitalSignModel] = []
    let endPacket: Data = Data(bytes: [12,13,14])
    var lastRealTimeData: NXVitalSignModel?
    var lastUpdateTime: Date = Date()
    var delegate: NXBluetoothNotifier?
    
    func didUpdateValueFor(_ characteristic: CBCharacteristic) {
        
        guard let value = characteristic.value else {
            return
        }
        
        if characteristic.uuid == NXCharacteristicUUID.realTimeData {
            self.realTimeDataExtract(value)
            // TODO: Check last bit
        }
        if characteristic.uuid == NXCharacteristicUUID.offlineData {
            self.offlineDataExtract(value)
        }
    }
    
    func realTimeDataExtract(_ value: Data) {
        let vitalSign = self.parsePacketData(value, start: 0)
        delegate?.didUpdateValue(vitalSign)
        
        guard let last = lastRealTimeData else {
            self.lastRealTimeData = vitalSign
            return
        }
        
        if last != vitalSign {
            last.lastsFor = vitalSign.registerDate.timeIntervalSince1970 - last.registerDate.timeIntervalSince1970
            self.lastRealTimeData = vitalSign
            self.dataStoreHandler.save([last])
            self.analyseDataHandler.analyseRealTimeData(last)
        }
    }
    
    func saveLastData() {
        
        if let last = lastRealTimeData {
            last.lastsFor = Date().timeIntervalSince1970 - last.registerDate.timeIntervalSince1970
            self.lastRealTimeData = nil
            self.dataStoreHandler.save([last])
        }
        
    }
    
    func offlineDataExtract(_ value: Data) {
        
        // TODO: start syncing delegate
        
        if value == self.endPacket {
            self.dataStoreHandler.save(offlineData)
            self.analyseDataHandler.analyseOfflineData(offlineData)
            self.offlineData.removeAll()
            
            // TODO: end of syncing offline data delegate
        }
        
        var timeArray: [UInt8] = Array(repeating: 0, count: 4)
        value.copyBytes(to: &timeArray, from: Range(uncheckedBounds: (15,19)))
        let timeInterval = timeArray.withUnsafeBufferPointer {
            ($0.baseAddress!.withMemoryRebound(to: UInt32.self, capacity: 1) { $0 })
            }.pointee
        
        for i in 0..<5 {
            let data = self.parsePacketData(value, start: i * 3)
            data.registerDate = NSDate(timeIntervalSince1970: TimeInterval(timeInterval + UInt32(i)))
            data.isOffline = true
            data.lastsFor = NXSetting.NXBluetooth.offlineDataInterval
            self.deleteRepeatedValues(data)
        }
        
    }
    
    private func deleteRepeatedValues(_ data: NXVitalSignModel) {
        
        guard let last = offlineData.popLast() else {
            self.offlineData.append(data)
            return
        }
        
        if last == data {
            last.lastsFor += NXSetting.NXBluetooth.offlineDataInterval
        }
        
        offlineData.append(last)
        
    }
    
    
    static func checkRealmFileSize() {
        if let realmPath = Realm.Configuration.defaultConfiguration.fileURL?.relativePath {
            do {
                let attributes = try FileManager.default.attributesOfItem(atPath:realmPath)
                if let fileSize = attributes[FileAttributeKey.size] as? Double {
                    
                    print(fileSize, "file size")
                }
            }
            catch (let error) {
                print("FileManager Error: \(error)")
            }
        }
    }
 
    
    
    func parsePacketData(_ packet: Data, start: Int) -> NXVitalSignModel {
        
        var byte: UInt8 = 0
        
        packet.copyBytes(to: &byte, from: Range(uncheckedBounds: (start,start + 1)))
        let respiratory: UInt8 = byte
        
        
        // MARK: byte 1: temperature
        packet.copyBytes(to: &byte, from: Range(uncheckedBounds: (start + 1, start + 2)))
        let rawTemperature: UInt8 = byte
        let temperature: Float = -46.5 * (175.72 / 65536) * (Float(rawTemperature) * 32 + 26612)
        
        // MARK: byte 2: orientation and humidity
        packet.copyBytes(to: &byte, from: Range(uncheckedBounds: (start + 2, start + 3)))
        
        let orientation: UInt8 = (byte & 0x03)
        let humidity: UInt8 = ((byte >> 2) & 0x07)
        
        //print(temperature, humidity, respiratory, orientation, #function)
        
        
        return NXVitalSignModel(value: ["temperature": temperature, "respiratory": Int(respiratory), "humidity": Int(humidity), "orientation": Int(orientation)])
        
    }
    
}


protocol NXBluetoothNotifier {
    
    func didUpdateValue(_ vitalSign: NXVitalSignModel)
    
}
