//
//  NXSyncableModel.swift
//  ninix
//
//  Created by Ali on 4/22/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import RealmSwift

class NXSyncableModel: Object {
    
    dynamic var synced: Bool = false
    dynamic var registerDate = NSDate()
    
}
