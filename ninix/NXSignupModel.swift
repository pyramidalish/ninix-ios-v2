//
//  NXSignupModel.swift
//  ninix
//
//  Created by Ali on 4/22/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import ObjectMapper

struct NXSignupModel: Mappable {
    
    var mobile: String?
    var password: String?
    var fullName: String?
    var deviceSerial: String?
    
    init(mobile: String, password: String, fullName: String, deviceSerial: String) {
        self.mobile = mobile
        self.password = password
        self.fullName = fullName
        self.deviceSerial = deviceSerial
    }
    
    init?(map: Map) {
        
    }
 
    
    mutating func mapping(map: Map) {
        mobile       <- map["mobile"]
        password     <- map["password"]
        fullName     <- map["fullname"]
        deviceSerial <- map["device_serial"]
    }
    
}
