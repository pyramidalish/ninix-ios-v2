//
//  NXJsonResponse.swift
//  ninix
//
//  Created by Ali on 4/24/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import ObjectMapper

struct NXJsonResponse<Result: Mappable>: Mappable {
    
    var code: Int?
    var messageFa: String?
    var messageEn: String?
    var result: Result?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        self.code <- map["code"]
        self.messageFa <- map["message_fa"]
        self.messageEn <- map["message_en"]
        self.result <- map["result"]
    }
    
}
