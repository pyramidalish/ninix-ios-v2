//
//  NXHttp.swift
//  ninix
//
//  Created by Ali on 4/24/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

// base http protocol, all internet services should conform this protocol
protocol NXHttpBase {
    
    associatedtype ResultModel: Mappable
    
    var needAuth: Bool { get set }
    var url: URL { get set }
    
    func onSuccessResponse(response: ResultModel)
    func onFailureResponse(error: NXJsonResponse<ResultModel>)
    func onServerFail(_ error: String)
    func baseUrlRequest() -> URLRequest?
    func urlRequest() -> URLRequest?
    func accessTokenProblem()
    func request()
    
}

// get http protocol
protocol NXHttpGet: NXHttpBase {
    func getUrlRequest() -> URLRequest?
}

protocol NXHttpPost: NXHttpBase {
    var parameters: Parameters? { get set }
    func postUrlRequest() -> URLRequest?
}

extension NXHttpBase {
    
    func request() {
        
        if let urlRequest = self.urlRequest() {
            Alamofire.request(urlRequest).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    if let json = value as? Parameters, let response = NXJsonResponse<ResultModel>(JSON: json) {
                        
                        if response.code == 0, let result = response.result {
                            self.onSuccessResponse(response: result)
                        }
                        else {
                            self.onFailureResponse(error: response)
                        }
                    }
                    else {
                        self.onServerFail("Unknown Error")
                    }
                    
                case .failure(let error):
                    self.onServerFail(error.localizedDescription)
                }
            }
        }
        
    }
    
    func baseUrlRequest() -> URLRequest? {
        var urlRequest = URLRequest(url: url)
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if self.needAuth {
            guard let token = NXSecureStorage.getToken(), let authorization = token.authorizationHeader() else {
                return nil
            }
            urlRequest.addValue(authorization, forHTTPHeaderField: "Authorization")
        }
        return urlRequest
    }
    
    func accessTokenProblem() {
        print("access token problem")
    }
    
}

extension NXHttpGet {
    
    
    func getUrlRequest() -> URLRequest? {
        if var urlRequest = self.baseUrlRequest() {
            urlRequest.httpMethod = "GET"
            return urlRequest
        }
        self.accessTokenProblem()
        return nil
    }
    
    func urlRequest() -> URLRequest? {
        return self.getUrlRequest()
    }
    
}

extension NXHttpPost {
    
    func postUrlRequest() -> URLRequest? {
        if var urlRequest = self.baseUrlRequest(), let parameters = self.parameters {
            urlRequest.httpMethod = "POST"
            
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            }
            catch {
                
            }
            return urlRequest
        }
        return nil
    }
    
    func urlRequest() -> URLRequest? {
        return self.postUrlRequest()
    }
    
}
