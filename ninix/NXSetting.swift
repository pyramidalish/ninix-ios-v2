//
//  NXSetting.swift
//  ninix
//
//  Created by Ali on 4/24/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
struct NXSetting {
    
    private static let baseAddress: String = "https://api.ninixco.com/"
    
    struct NXBluetooth {
        static let discoveryInterval: TimeInterval = 15
        static let offlineDataInterval: TimeInterval = 10
    }
    
    struct NXCriticalPoint {
        
        struct Temperature {
            
            static let over: Float = 38.5
            static let max: Float = 38.0
            static let min: Float = 36.5
            static let under: Float = 36.0
            static let time: TimeInterval = 60 * 30
            
        }
        
        struct Respiratory {
            
            static let over: Int = 32
            static let max: Int = 28
            static let min: Int = 22
            static let under: Int = 18
            static let time: TimeInterval = 30
            
        }
        
    }
    
    struct NXHttp {
        
        struct NXAddress {
            
            static let signup: String = NXSetting.baseAddress.appending("user/register")
            static let babySync: String = NXSetting.baseAddress.appending("baby/sync")
            static let login: String = NXSetting.baseAddress.appending("user/login")
            static let userSync: String = NXSetting.baseAddress.appending("user/sync")
            static let notificationSync: String = NXSetting.baseAddress.appending("notification/sync")
            static let streamData: String = NXSetting.baseAddress.appending("stream/send")
            
        }
        
    }
    
}
