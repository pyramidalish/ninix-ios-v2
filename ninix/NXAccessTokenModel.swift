//
//  NXAccessTokenModel.swift
//  ninix
//
//  Created by Ali on 4/24/17.
//  Copyright © 2017 ninix. All rights reserved.
//

import Foundation
import ObjectMapper

class NXAccessTokenModel: NSObject, NSCoding, Mappable {
    
    var accessToken: String?
    var refreshToken: String?
    var tokenType: String?
    var expiresIn: Date?
    
    required init?(coder aDecoder: NSCoder) {
        self.accessToken = aDecoder.decodeObject(forKey: "access_token") as? String
        self.refreshToken = aDecoder.decodeObject(forKey: "refresh_token") as? String
        self.tokenType = aDecoder.decodeObject(forKey: "token_type") as? String
        self.expiresIn = aDecoder.decodeObject(forKey: "expires_in") as? Date
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.accessToken, forKey: "access_token")
        aCoder.encode(self.refreshToken, forKey: "refresh_token")
        aCoder.encode(self.tokenType, forKey: "token_type")
        aCoder.encode(self.expiresIn, forKey: "expires_in")
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        accessToken  <- map["access_token"]
        refreshToken <- map["refresh_token"]
        tokenType    <- map["token_type"]
        expiresIn    <- (map["expires_in"], ExpireDateTransform())
    }
    
    func save() {
        NXSecureStorage.set(self, for: .token)
    }
    
    func remove() {
        NXSecureStorage.remove(.token)
    }
    
    func authorizationHeader() -> String? {
        
        if let tokenType = self.tokenType, let accessToken = self.accessToken, let expiresIn = self.expiresIn, expiresIn > Date() {
            return "\(tokenType) \(accessToken)"
        }
        
        // TODO: handle expired token
        
        return nil
    }
    
}

class ExpireDateTransform: TransformType {
    public typealias Object = Date
    public typealias JSON = Double
    
    public init() {}
    
    open func transformFromJSON(_ value: Any?) -> Date? {
        if let timeInt = value as? Double {
            return Date(timeIntervalSince1970: Date().timeIntervalSince1970 + TimeInterval(timeInt))
        }
        
        if let timeStr = value as? String {
            return Date(timeIntervalSince1970: Date().timeIntervalSince1970 + TimeInterval(atof(timeStr)))
        }
        
        return nil
    }
    
    open func transformToJSON(_ value: Date?) -> Double? {
        if let date = value {
            return Double(date.timeIntervalSince1970)
        }
        return nil
    }
}
