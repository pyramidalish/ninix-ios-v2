//
// Created by Ali on 4/19/17.
// Copyright (c) 2017 ninix. All rights reserved.
//

import UIKit
import CoreBluetooth

class NXDiscoveredDevice: NSObject {

    var name: String
    var time: TimeInterval
    var rssi: NSNumber
    var peripheral: CBPeripheral

    init(name: Any?, time: TimeInterval, rssi: NSNumber, peripheral: CBPeripheral) {
        self.name = (name as? String) ?? "undefined"
        self.time = time
        self.rssi = rssi
        self.peripheral = peripheral
    }


    static func == (left: NXDiscoveredDevice, right: NXDiscoveredDevice) -> Bool {
        return left.name == right.name && left.peripheral == right.peripheral
    }

    static func != (left: NXDiscoveredDevice, right: NXDiscoveredDevice) -> Bool {
        return !(left == right)
    }

}